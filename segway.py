#!/usr/bin/python3
import numpy as np
import subprocess
from mpu6050 import *
from time import time, sleep
from gpiozero import RGBLED, Motor
from math import pi

import torch
import torch.nn as nn
import torch.nn.functional as F

from agent import q_agent

# tet = 0, 0.1316, 0.1980, 0.0029

angleTolerance = 0.15
maxAngle = 0.35

discreteLevel = 10
layerSize = 32
inputParam = 5

encoderPath = "encoder.txt"
networkPath = "data/network.pt"
logPath = "log.txt"

class LedInfo(RGBLED):
    def __init__(self, rp, gp, bp):
        super(LedInfo, self).__init__(rp, gp, bp)
    
    def __call__(self, comand):
        if len(comand) < 1:
            return
        color = comand[0]
        if color == 'r':
            self.value = (1, 0, 0)
        elif color == 'g':
            self.value = (0, 1, 0)
        elif color == 'b':
            self.value = (0, 0, 1)
        elif color == 'y':
            self.value = (1, 1, 0)
        elif color == 'w':
            self.value = (1, 1, 1)
        elif color == '-':
            self.value = (0, 0, 0)
        if len(comand) >= 2:
            t = 1
            t_ = 0.2
            if len(comand) > 2:
                t = t_ = float(comand[2:])
            if comand[1] == '*':
                sleep(t_)
                self.value = (0, 0, 0)
                sleep(t_)
            if comand[1] == '+':
                sleep(t)
                self.value = (0, 0, 0)

class Segway(q_agent):
    def __init__(self):
        self.useEncoder = True
        self.epsilon = 0
        self.led = LedInfo(21, 26, 20)
        self.lMotor = Motor(7,1)
        self.rMotor = Motor(25, 8)
        self.scale = 1
        self.diam = 0.042

        self.mpu = None
        while self.mpu is None:
            try:
                self.mpu = MPU6050()
                self.mpu.initialize()
            except Exception:
                print("MPU connect error!")
                self.led("y*")
        self.mpu.angleCorrect = 0.03
        # self.mpu.angleCorrect = -0.11 

        self.phi, self.dphi = 0, 0
        self.xl, self.xr = 0., 0.
        self.dxl, self.dxr = 0., 0.
        self.action = 0

        self.led("y")
        done = False
        while not done:
            try:
                super().__init__()
                done = True
            except Exception as e:
                print(e)

        try:
            self.network.load_state_dict(torch.load(networkPath))
        except Exception as e:
            while True:
                self.led("r*")

        self.led("w")
        with open("log.txt", 'w') as f:
            ...

        self.startTime = time()

    def readSensor(self):
        self.phi, self.dphi = self.mpu()
        with open(encoderPath) as f:
            enc = f.read().split()
            if len(enc) == 4:
                self.xl = float(enc[0])
                self.xr = float(enc[1])
                self.dxl = float(enc[2])
                self.dxr = float(enc[3])
                self.x = ((self.xl+self.xr)*0.5)/360*pi*self.diam
                self.dx = ((self.dxl+self.dxr)*0.5)/360*pi*self.diam

    def aplyControl(self, pow):
        self.lMotor.value = pow
        self.rMotor.value = pow

    def getAction(self, u = 0):
        state = torch.tensor([self.x, self.phi, self.dx, self.dphi, u], dtype=torch.float32)
        self.action = torch.argmax(self.network(state).detach())
        if self.epsilon!=0 and np.random.binomial(1,p=self.epsilon):
            action = np.random.choice(range(discreteLevel*2 + 1))
        self.control = (float(self.action) - discreteLevel)/discreteLevel
        return self.control

    def __call__(self, u = 0):
        self.readSensor()
        self.aplyControl(self.getAction(u))
        self.log()
        return abs(self.phi) > maxAngle

    def __str__(self):
        self.mpu.reset()
        self.readSensor()
        return "phi: {:>6.3f} dphi: {:>8.3f} x: {:>3.1f} dx: {:>3.1f}".format(\
            self.phi, self.dphi, \
            (self.xl+self.xr)*0.5, (self.dxl+self.dxr)*0.5)

    def resetLog():
        with open("log.txt",'w') as f:
            ...
            
    def log(self, end=False):
        with open("log.txt",'a') as f:
            if end:
                f.write("<t time=\"{:8.4}\" x=\"{:8.4}\" phi=\"{:8.4}\" dx=\"{:8.4}\" dphi=\"{:8.4}\" u=\"{:8.4}\">\n".format(\
                    0, 0, 0, 0, 0, 0))
            else:
                f.write("<t time=\"{:8.4}\" x=\"{:8.4}\" phi=\"{:8.4}\" dx=\"{:8.4}\" dphi=\"{:8.4}\" u=\"{:8.4}\">\n".format(\
                    time() - self.startTime, self.x, self.phi, self.dx, self.dphi, self.control))

    def go(self):
        self.aplyControl(0)
        ps = subprocess.Popen("encoder/encoder")
        self.mpu.reset()
        try:
            ready = False
            while not ready:
                self.led('y')
                self.readSensor()
                if abs(self.phi) < angleTolerance:
                    for i in range(3):
                        self.led('g*')
                    self.readSensor()

                ready = abs(self.phi) < angleTolerance

            print("start phi = {}".format(self.phi))
            self.led('g')

            f = False
            while not f:
                f = self()

        except Exception as e:
            print(e)
        finally:
            self.aplyControl(0)
            ps.kill()
            print("last phi = {}".format(self.phi))
            self.led('r+')

    def calibMPU(self):
        for i in range(3):
            self.led('r*')
        self.led('g')
        s = 0
        for i in range(100):
            self.readSensor()
            s += self.phi
            sleep(0.03)
        self.led('-')

        return s/100+self.mpu.angleCorrect

    def test(self, motor=0):
        p = 0
        t = time()
        ps = subprocess.Popen("encoder/encoder")
        try:
            while True:
                print(self)
                self.aplyControl(p)
                if time()-t>2:
                    p = motor - p
                    t = time()
                sleep(0.05)
        except Exception as e:
            print(e)
        finally:
            ps.kill()
            self.aplyControl(0)

    def reward(self, s, r, u=0, a=None):
        return max(0,1-(s[1])**2)

    def step(self, u):
        done = self()
        next_s = [self.x, self.phi, self.dx, self.dphi]
        r = self.reward(next_s, 1, u, self.control)
        return next_s, r, done, self.control

    def train_simple(self, u, t_max=1000, train=True):
        total_reward = 0
        s = [self.x, self.phi, self.dx, self.dphi]
        for t in range(t_max):
            next_s, r, done, a = self.step(u)
            if train:
                self.opt.zero_grad()
                loss = self.compute_td_loss([s], [a], u, [t], [r], [next_s], [done])
                loss.backward()
                self.opt.step()
            total_reward += r
            s = next_s
            if done:
                self.aplyControl(0)
                break
        return np.array([total_reward, float(loss)])

    def train_batch(self, u, t_max=1000, train=True):
        total_reward = 0
        s = [self.x, self.phi, self.dx, self.dphi]
        S = []
        A = []
        T = []
        R = []
        NS = []
        Done = []
        for t in range(t_max):
            next_s, r, done, a = self.step(u)
            S.append(s)
            A.append(a)
            T.append(t)
            R.append(r)
            NS.append(next_s)
            Done.append(done)
            total_reward += r
            s = next_s
            if done:
                self.aplyControl(0)
                break

        if train:
            self.led('b')
            self.opt.zero_grad()
            loss = self.compute_td_loss([s], [a], u, [t], [r], [next_s], [done])
            loss.backward()
            self.opt.step()
            self.led('-')
        return np.array([total_reward, float(loss)])

    def train(self, type=0):
        self.epsilon = 0.1
        u = 0
        self.mpu.reset()
        self.aplyControl(0)
        try:
            while True:
                ready = False
                while not ready:
                    self.led('y')
                    self.readSensor()
                    if abs(self.phi) < angleTolerance:
                        for i in range(3):
                            self.led('g*')
                            self.readSensor()
                    ready = abs(self.phi) < angleTolerance

                self.led('g')
                for i in range(5):
                    self.readSensor()
                    sleep(0.05)
                if self.useEncoder:
                    ps = subprocess.Popen("encoder/encoder")
                if type==0:
                    reward, loss = self.train_simple(u)
                else:
                    reward, loss = self.train_batch(u)
                # self.log(end=True)
                self.led('r+0.3')
                print("reward={:>7.3f}, loss={:>9.5f}".format(reward, loss))
                if self.useEncoder:
                    ps.kill()
                self.led('-')
        except Exception as e:
            print(e)
        finally:
            self.aplyControl(0)
            self.led('r+2')



'''
from segway import Segway
s = Segway()
s.train()
'''