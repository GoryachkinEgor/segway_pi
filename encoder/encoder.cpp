#include <wiringPi.h>
#include <stdio.h>
#include <fstream>

int lPulse1 = 4;
int lPulse2 = 5;
int rPulse1 = 2;
int rPulse2 = 3;
int lState = 0;
int rState = 0;
int lOldState = 0;
int rOldState = 0;
double lSpeed = 0;
double rSpeed = 0;

void lPulse1Collback()
{
    if(digitalRead(lPulse1) == LOW)
        lState += digitalRead(lPulse2) ? 1 : -1;
    else
        lState += digitalRead(lPulse2) ? -1 : 1;
}

void lPulse2Collback()
{
    if(digitalRead(lPulse2) == LOW)
        lState += digitalRead(lPulse1) ? -1 : 1;
    else
        lState += digitalRead(lPulse1) ? 1 : -1;
}

void rPulse1Collback()
{
    if(digitalRead(rPulse1) == LOW)
        rState += digitalRead(rPulse2) ? 1 : -1;
    else
        rState += digitalRead(rPulse2) ? -1 : 1;
}

void rPulse2Collback()
{
    if(digitalRead(rPulse2) == LOW)
        rState += digitalRead(rPulse1) ? -1 : 1;
    else
        rState += digitalRead(rPulse1) ? 1 : -1;
}

int main()
{
    wiringPiSetup();
    pinMode(lPulse1, INPUT);
    pinMode(lPulse2, INPUT);
    pinMode(rPulse1, INPUT);
    pinMode(rPulse2, INPUT);
    wiringPiISR(lPulse1, INT_EDGE_BOTH, lPulse1Collback);
    wiringPiISR(lPulse2, INT_EDGE_BOTH, lPulse2Collback);
    wiringPiISR(rPulse1, INT_EDGE_BOTH, rPulse1Collback);
    wiringPiISR(rPulse2, INT_EDGE_BOTH, rPulse2Collback);

    unsigned int oldTime = millis();
    unsigned int newTime = 0;
    delay(1);

    std::ofstream file;
    while(1)
    {
        newTime = millis();
        lSpeed = (double)(lState - lOldState) / (newTime - oldTime) * 1000;
        rSpeed = (double)(rState - rOldState) / (newTime - oldTime) * 1000;

        lOldState = lState;
        rOldState = rState;
        // printf("%6.1f %6.1f %6.1f %6.1f\n", (double)lState/2, (double)rState/2, (double)lSpeed/2, (double)rSpeed/2);
        file.open("encoder.txt");
        file << (double)lState/2 << ' ';
        file << (double)rState/2 << ' ';
        file << (double)lSpeed/2 << ' ';
        file << (double)rSpeed/2 << ' ';
        oldTime = newTime;
        file.close();
        delay(50);
    }
}

