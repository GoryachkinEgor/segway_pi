import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

class base_agent(nn.Module):
    def __init__(self):
        super().__init__()
        self.network = nn.Sequential()
        self.network.add_module('layer1', nn.Linear(5, 32))
        self.network.add_module('relu1', nn.ReLU())
        self.network.add_module('layer2', nn.Linear(32, 32))
        self.network.add_module('relu2', nn.ReLU())
        self.network.add_module('layer3', nn.Linear(32, 21))

        self.opt = torch.optim.Adam(self.network.parameters(), lr=1e-3)
        self.epsilon = 0.1

    def reward(self, s, r, u=0):
        return r

    def save(self, name):
        torch.save(self.network.state_dict(), name)

    def load(self, name):
        self.network.load_state_dict(torch.load(name))

class q_agent(base_agent):
    def __init__(self):
        super().__init__()

    def reward(self, s, r, u=0, a=None):
        # return r + min(0, (1-s[1])**2)
        return 1-min(1,0.5*(s[2]-u)**2)
        # return r

    def get_action(self, state, u, epsilon=0):
        state = torch.tensor((np.hstack([state, u]))[None], dtype=torch.float32)
        q_values = self.network(state).detach()
        action = torch.argmax(q_values)
        if np.random.binomial(1,p=epsilon):
            action = np.random.choice(range(q_values.shape[-1]))

        return int(action)

    def compute_td_loss(self, states, actions, control, t, rewards, next_states, is_done, gamma=0.99, check_shapes=False):
        states = np.array(states)
        next_states = np.array(next_states)
        actions = torch.tensor(actions, dtype=torch.long)    # shape: [batch_size]
        rewards = torch.tensor(rewards, dtype=torch.float32)  # shape: [batch_size]
        is_done = torch.tensor(is_done, dtype=torch.uint8)  # shape: [batch_size]

        state_network_in = torch.tensor(np.hstack([states,[[control] for i in range(len(states))]]), dtype=torch.float32)
        next_states_network_in = torch.tensor(np.hstack([np.array(next_states),[[control] for i in range(len(states))]]), dtype=torch.float32)
        predicted_qvalues = self.network(state_network_in)
        predicted_qvalues_for_actions = predicted_qvalues[range(states.shape[0]), actions]
        predicted_next_qvalues = self.network(next_states_network_in)
        next_state_values =  torch.max(predicted_next_qvalues, dim=-1)[0]
        assert next_state_values.dtype == torch.float32
        target_qvalues_for_actions =  rewards + gamma*next_state_values
        target_qvalues_for_actions = torch.where(
            is_done, rewards, target_qvalues_for_actions)
        loss = torch.mean((predicted_qvalues_for_actions -
                           target_qvalues_for_actions.detach()) ** 2)
        if check_shapes:
            assert predicted_next_qvalues.data.dim(
            ) == 2, "make sure you predicted q-values for all actions in next state"
            assert next_state_values.data.dim(
            ) == 1, "make sure you computed V(s') as maximum over just the actions axis and not all axes"
            assert target_qvalues_for_actions.data.dim(
            ) == 1, "there's something wrong with target q-values, they must be a vector"

        return loss
